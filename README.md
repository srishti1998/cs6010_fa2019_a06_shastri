##SECTIONS

[REFLECTION](##reflection)

[REFLECTION QUESTIONS](##reflection-questions)


## *REFLECTION*
This is a indiviual assignment in which we were given 4 exercises to implement apply() and groupby() operations on different datasets. Each exercise was having some steps for which we have to analyze the data and then write the code in order to complete them.
Apply() takes function and applies it to every value of the series and returns series with applied operation. Groupby() splits the data based on some criteria then apply functions on that data and then finally returns the result. 
I  also used other technologies in this assignment such as  jupyterlab and python  for completing all the exercises and git for version control.


## *REFLECTION QUESTIONS*
>*Q1. What do I believe I did well on this assignment?*

I think I was able to perform apply() operation well. I found it easy as compared to groupby() operation.


>*Q2. What was the most challenging part of this assignment?*

The most challenging part was to group the data based on certain criteria using groupby() operation.


>*Q3. What would have made this assignment a better experience?*

This assignment was  a better experience because I was able to perform different operations on the data and generate different modified datasets.



>*Q4. What do I need help with?*

I don't think there was anything very tough in this assignment, it was quite easy task as compared to other assignments. Although I need to work a little bit with the groupby() operation.

